# coding=utf-8

import sys
import serial
import time
import subprocess
import pythoncom
from pycaw.pycaw import AudioUtilities
import threading

from PyQt5.QtWidgets import QWidget, QDesktopWidget, QApplication, QDialog, QLineEdit, QPushButton, QListWidgetItem, QListWidget
from PyQt5 import QtCore, QtGui
from PyQt5.QtCore import pyqtSlot


class MyDialog(QDialog):
    def __init__(self, parent=None):
        super(MyDialog, self).__init__(parent)
        self.resize(290, 200)
        listWidget = QListWidget()
        listWidget.resize(100, 100)
        listWidget.move(20, 20)
        listWidget.show()

        ls = ["1", "2", "3", "4"]

        listWidget.addItems(ls)


class Window(QWidget):
    sig = QtCore.pyqtSignal(int)

    def __init__(self, slider_1: str, slider_2: str, slider_3: str, slider_4: str):
        super().__init__()
        self.slider_1 = slider_1
        self.slider_2 = slider_2
        self.slider_3 = slider_3
        self.slider_4 = slider_4
        self.initUI()

    def initUI(self):
        self.resize(290, 200)
        self.center()

        self.p_slider_1 = QLineEdit(self)
        self.p_slider_1.move(20, 20)
        self.p_slider_1.resize(250, 20)
        self.p_slider_1.setText(self.slider_1)

        self.p_slider_2 = QLineEdit(self)
        self.p_slider_2.move(20, 50)
        self.p_slider_2.resize(250, 20)
        self.p_slider_2.setText(self.slider_2)

        self.p_slider_3 = QLineEdit(self)
        self.p_slider_3.move(20, 80)
        self.p_slider_3.resize(250, 20)
        self.p_slider_3.setText(self.slider_3)

        self.p_slider_4 = QLineEdit(self)
        self.p_slider_4.move(20, 110)
        self.p_slider_4.resize(250, 20)
        self.p_slider_4.setText(self.slider_4)

        self.button = QPushButton('Save', self)
        self.button.move(35, 150)
        self.button.resize(100, 20)
        self.button.clicked.connect(self.on_click)

        self.button_prc = QPushButton('Process List', self)
        self.button_prc.move(145, 150)
        self.button_prc.resize(100, 20)
        self.button_prc.clicked.connect(self.on_pushButton_clicked)

        self.dialogTextBrowser = MyDialog(self)

        self.setWindowTitle('Volume Mixer')
        self.show()

    def on_click(self):
        self.slider_1 = self.p_slider_1.text()
        self.slider_2 = self.p_slider_2.text()
        self.slider_3 = self.p_slider_3.text()
        self.slider_4 = self.p_slider_4.text()
        f = open('settings.txt', 'w+')
        f.write(self.slider_1 + '\n')
        f.write(self.slider_2 + '\n')
        f.write(self.slider_3 + '\n')
        f.write(self.slider_4)

    @QtCore.pyqtSlot()
    def on_pushButton_clicked(self):
        self.dialogTextBrowser.exec_()

    def center(self):
        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())


class ExamplePopup(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.name = "dada"
        self.label = QLabel(self.name, self)


settings_file = open('settings.txt', 'r')
settings_file_line = settings_file.readlines()

app = QApplication(sys.argv)
W = Window(slider_1=settings_file_line[0].strip(),
           slider_2=settings_file_line[1].strip(),
           slider_3=settings_file_line[2].strip(),
           slider_4=settings_file_line[3].strip())
settings_file.close()


def vol_mixer():
    ser = serial.Serial('COM4', 19200)

    def remap(x, oMin, oMax, nMin, nMax):
        if x < 5:
            x = 0
        # range check
        if oMin == oMax:
            return None

        if nMin == nMax:
            return None

        # check reversed input range
        reverseInput = False
        oldMin = min(oMin, oMax)
        oldMax = max(oMin, oMax)
        if not oldMin == oMin:
            reverseInput = True

        # check reversed output range
        reverseOutput = False
        newMin = min(nMin, nMax)
        newMax = max(nMin, nMax)
        if not newMin == nMin:
            reverseOutput = True

        portion = (x - oldMin) * (newMax - newMin) / (oldMax - oldMin)
        if reverseInput:
            portion = (oldMax - x) * (newMax - newMin) / (oldMax - oldMin)

        result = portion + newMin
        if reverseOutput:
            result = newMax - portion

        return result

    def checkValue(name, value):
        if name == "volMain":
            MasterVolume(value)
        if name == 'vol1':
            AppVolume_1(value)
        if name == 'vol2':
            AppVolume_2(value)
        if name == 'vol3':
            AppVolume_3(value)
        if name == 'vol4':
            AppVolume_4(value)
        if name == 'muteMode':
            MuteVolume(value)
        if name == 'micMode':
            MuteMic(value)
        return

    def MasterVolume(value):
        try:
            val = str(remap(float(value), 0, 1023, 0, 65535))
            subprocess.Popen('nircmd.exe setvolume 0 ' + val + ' ' + val)
        except ValueError:
            print("Error: Master Volume")

    def AppVolume_1(value):
        pythoncom.CoInitialize()
        try:
            sessions = AudioUtilities.GetAllSessions()
            for session in sessions:
                volume = session.SimpleAudioVolume
                sl = W.slider_1.split(',')
                for i in range(len(sl)):
                    if session.Process and session.Process.name() == sl[i].strip():
                        volume.SetMasterVolume(
                            remap(float(value), 0, 1023, 0, 1), None)
        except ValueError:
            print("Error: 1 App Volume")

    def AppVolume_2(value):
        pythoncom.CoInitialize()
        try:
            sessions = AudioUtilities.GetAllSessions()
            for session in sessions:
                volume = session.SimpleAudioVolume
                sl = W.slider_2.split(',')
                for i in range(len(sl)):
                    if session.Process and session.Process.name() == sl[i].strip():
                        volume.SetMasterVolume(
                            remap(float(value), 0, 1023, 0, 1), None)
        except ValueError:
            print("Error: 2 App Volume")

    def AppVolume_3(value):
        pythoncom.CoInitialize()
        try:
            sessions = AudioUtilities.GetAllSessions()
            for session in sessions:
                volume = session.SimpleAudioVolume
                sl = W.slider_3.split(',')
                for i in range(len(sl)):
                    if session.Process and session.Process.name() == sl[i].strip():
                        volume.SetMasterVolume(
                            remap(float(value), 0, 1023, 0, 1), None)
        except ValueError:
            print("Error: 3 App Volume")

    def AppVolume_4(value):
        pythoncom.CoInitialize()
        try:
            sessions = AudioUtilities.GetAllSessions()
            for session in sessions:
                volume = session.SimpleAudioVolume
                sl = W.slider_4.split(',')
                for i in range(len(sl)):
                    if session.Process and session.Process.name() == sl[i].strip():
                        volume.SetMasterVolume(
                            remap(float(value), 0, 1023, 0, 1), None)
        except ValueError:
            print("Error: 4 App Volume")

    def MuteVolume(value):
        try:
            subprocess.Popen('nircmd.exe mutesysvolume ' + str(value))
        except MuteError:
            print("Error: Mute Volume")

    def MuteMic(value):
        subprocess.Popen('nircmd.exe mutesysvolume ' +
                         str(value) + ' "Микрофон"')

    thr = threading.currentThread()
    while getattr(thr, "do_run", True):
        if ser != None:
            try:
                line = ser.readline().decode("utf-8").split(':')
                print(line[0] + ':' + line[1][1:-2])
                checkValue(line[0], line[1][1:-2])
            except:
                ser = None
                print('Disconnected!')
        else:
            time.sleep(2)
            try:
                ser = serial.Serial('COM4', 19200)
                print('Connected!')
            except:
                print('Still disconnected')


t = threading.Thread(target=vol_mixer)
t.start()


def stop_system():
    t.do_run = False
    t.join()


sys.exit(app.exec_(), stop_system())
