import wmi
from collections import OrderedDict
from itertools import repeat

c = wmi.WMI ()
proc_list = []

for process in c.Win32_Process ():
  proc_list.append(process.Name)

unique_list = list(OrderedDict(zip(proc_list, repeat(None))))
print(unique_list)