# Arduino Volume Mixer

![Render](https://gitlab.com/BlackRockSoul/volume_mixer/raw/5ba6f1287147c79d86f7a01890e7fbb38f3cd204/images/Volume_Mixer_1.png)

### Dependencies
* Arduino
* [Python](https://www.python.org/) v3.6+
* [pycaw](https://github.com/AndreMiras/pycaw)


### Running

* Upload firmware in Arduino
* Start python script

```sh
$ cd Volume_Mixer
$ python mixer.py
```

##### Photo
![Render](https://gitlab.com/BlackRockSoul/volume_mixer/raw/5ba6f1287147c79d86f7a01890e7fbb38f3cd204/images/Volume_Mixer_2.png)
![Render](https://gitlab.com/BlackRockSoul/volume_mixer/raw/5ba6f1287147c79d86f7a01890e7fbb38f3cd204/images/Volume_Mixer_3.png)