#include <ResponsiveAnalogRead.h>

// **************************************** DEFINE LEDS ***********************
const int ledNum = 7;
const int ledArray[] = {10, 12, 11, 9, 6, 5, 3};
enum ledByName
{
    PIN_MIC,
    PIN_MAIN_RED,
    PIN_MAIN,
    PIN_1,
    PIN_2,
    PIN_3,
    PIN_4
};
// **************************************** DEFINE LEDS ***********************

// **************************************** DEFINE BUTTONS ********************
const int BUTTON_PIN_MIC = 8;   //DIGITAL
const int BUTTON_PIN_MUTE = 7;  //DIGITAL
const int BUTTON_PIN_NIGHT = 2; //DIGITAL
// **************************************** DEFINE BUTTONS ********************

// **************************************** DEFINE POTENTIOMETERS *************
const int potePins[] = {A1, A2, A3, A4, A5};
const uint16_t poteName[] = {1, 2, 3, 4, 5};
// **************************************** DEFINE POTENTIOMETERS *************

// **************************************** POTENTIOMETER SMOOTHING ***********
ResponsiveAnalogRead *potes[5];
// **************************************** POTENTIOMETER SMOOTHING ***********

// **************************************** DEFAULT VALUES *********************
int micMode = false;
int MIC_READING;
int MIC_PREV = LOW;

bool muteMode = false;
int MUTE_READING;

bool nightMode = false;
bool MIC_Inverse = false;

long int lastUpdate = 0;

int body = 0, header = 0;
// **************************************** DEFAULT VALUES *********************

void setup()
{
    Serial.begin(19200); //Serial started
    for (int i = 0; i <= 4; i++)
    {
        potes[i] = new ResponsiveAnalogRead(potePins[i], true);
        potes[i]->update();
    }

    //Define led ports as OUTPUT
    for (int i = 0; i != ledNum - 1; i++)
        pinMode(ledArray[i], 0x1);
    //Define led ports as OUTPUT

    //Define button ports as INPUT
    pinMode(BUTTON_PIN_MIC, INPUT_PULLUP);
    pinMode(BUTTON_PIN_MUTE, INPUT_PULLUP);
    pinMode(BUTTON_PIN_NIGHT, INPUT_PULLUP);
    //Define button ports as INPUT

    button_handler(true);
    led_update(); //Update leds to be sure
}

void loop()
{
    readSerial();
    button_handler(false);
    value_handler();
    led_update();
}

void sendPacket(int header, int body)
{
    byte packet[] = {0xFF, header, body >> 8, body};
    Serial.write(packet, 4);
}

byte boolToByte(bool b)
{
    if (b)
        return (byte)1;
    else
        return (byte)0;
}

byte boolsToByte4(bool b1 = false, bool b2 = false, bool b3 = false, bool b4 = false)
{
    byte x = boolToByte(b1) << 0;
    x |= boolToByte(b2) << 1;
    x |= boolToByte(b3) << 2;
    x |= boolToByte(b4) << 3;
    return x;
}

void initialUpdate()
{
    for (int i = 0; i <= 4; i++)
    {
        potes[i]->update();
        sendPacket(poteName[i], potes[i]->getValue());
    }

    sendPacket(6, boolsToByte4(micMode, muteMode));
    sendPacket(7, boolsToByte4(nightMode, MIC_Inverse));
}

void updatePotes()
{
    for (int i = 0; i <= 4; i++)
    {
        potes[i]->update();
    }
}

void value_handler()
{
    if (millis() - lastUpdate < 66 || muteMode == true)
        return;

    updatePotes();
    for (int i = 0; i <= 4; i++)
    {
        if (potes[i]->hasChanged())
        {
            sendPacket(poteName[i], potes[i]->getValue());
        }
    }
    lastUpdate = millis();
}

void button_handler(bool start)
{
    if (muteMode == false)
    {
        MIC_READING = digitalRead(BUTTON_PIN_MIC);

        if (MIC_READING == HIGH && ((micMode == true && MIC_Inverse == false) || (micMode == false && MIC_Inverse == true) || start == true))
        {
            if (MIC_Inverse == false)
            {
                micMode = false;
            }
            else
            {
                micMode = true;
            }
            sendPacket(6, boolsToByte4(micMode, muteMode));
        }
        else if (MIC_READING == LOW && ((micMode == false && MIC_Inverse == false) || (micMode == true && MIC_Inverse == true)))
        {
            if (MIC_Inverse == true)
            {
                micMode = false;
            }
            else
            {
                micMode = true;
            }
            sendPacket(6, boolsToByte4(micMode, muteMode));
        }
    }

    MUTE_READING = digitalRead(BUTTON_PIN_MUTE);

    if (MUTE_READING == HIGH && (muteMode == true || start == true))
    {
        muteMode = false;
        sendPacket(6, boolsToByte4(micMode, muteMode));
    }
    else if (MUTE_READING == LOW && muteMode == false)
    {
        muteMode = true;
        sendPacket(6, 3);
    }

    if (digitalRead(BUTTON_PIN_NIGHT) == LOW && nightMode == true)
    {
        nightMode = false;
        sendPacket(7, boolsToByte4(nightMode, MIC_Inverse));
    }
    else if (digitalRead(BUTTON_PIN_NIGHT) == HIGH && nightMode == false)
    {
        nightMode = true;
        sendPacket(7, boolsToByte4(nightMode, MIC_Inverse));
    }
}

void led_update()
{
    if (nightMode == true)
    {
        for (int i = 0; i <= ledNum - 1; i++)
            digitalWrite(ledArray[i], 0x0);
        return;
    }

    if (muteMode == false && not potes[0]->getValue() == 0)
    {
        digitalWrite(ledArray[PIN_MAIN_RED], LOW);
        analogWrite(ledArray[PIN_MAIN], potes[0]->getValue() / 5);

        for (int i = 1; i <= 4; i++)
        {
            analogWrite(ledArray[i + 2], potes[i]->getValue() / 10);
        }
    }
    else
    {
        for (int i = 2; i <= ledNum - 1; i++)
            digitalWrite(ledArray[i], 0x0);
        digitalWrite(ledArray[PIN_MAIN_RED], HIGH);
    }

    if (micMode == true || muteMode == true)
    {
        digitalWrite(ledArray[PIN_MIC], HIGH);
    }
    else
    {
        digitalWrite(ledArray[PIN_MIC], LOW);
    }
}

void readSerial()
{
    byte packet[3] = {0x00};
    if (Serial.available() >= 4)
    {
        if ((byte)Serial.read() != 0xFF)
            return;

        for (int i = 0; i < 3; i++)
        {
            packet[i] = (byte)Serial.read();
        }

        int h = (int)packet[0];
        int b = (int)(packet[1] << 8) | packet[2];

        switch (h)
        {
        case 99:
            initialUpdate();
            break;
        case 1:
            MIC_Inverse = b;
            break;
        default:
            break;
        }
    }
}